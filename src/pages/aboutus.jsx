import React from 'react';
import {
  Page,
  Navbar,
  Block} from 'framework7-react';


const AboutUsPage = () => (
  <Page name="aboutus">
    <Navbar title="About us" />

    <Block className='apropos'>
    <div>
    This quiz was created by Maroine and Emy, as part of a mobile technology project.
    </div>

    
    </Block>
  </Page>
);

export default AboutUsPage;
