import { Block, Button, useStore } from "framework7-react";
import React from "react";
import store from "../js/store";

const LevelsPage = () => {
  const continents = [
    "North America",
    "Africa",
    "Europe",
    "Asia",
    "South America",
    "Oceania",
    "All continents",
  ];

  function modificationlevel(newlevel) {
    store.dispatch("setLevel", {value : newlevel});
  };
  return (
    <Block className="level-section">
    <h2 className="select-continent">
      On which continent do you want to test your knowledge?
    </h2>
    <div>
      {continents.map((continent) => (
        <Button
          className="continent"
          key={continent}
          onClick={function () {
            modificationlevel(continent);
          }}
        >
          {continent}
        </Button>
      ))}
    </div>
  </Block>
  );
};

export default LevelsPage;
