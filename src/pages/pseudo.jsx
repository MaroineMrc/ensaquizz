import { Block, Input } from "framework7-react";
import React from "react";

const PseudoPage = () => {

  

  return (
    <Block className="card">
    <div className="Pseudo">Enter your username:</div>
    <Input
      name="pseudo"
      label="Pseudo"
      type="text"
      placeholder="Your username"
      id="pseudo"
      clearButton
    ></Input>
  </Block>
              
  );
};

export default PseudoPage;