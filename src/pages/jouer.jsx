import React, { useState } from "react";
import { Page, Navbar, List, Block, Button, useStore } from "framework7-react";

import store from "../js/store";
import PseudoPage from "./pseudo";
import LevelsPage from "./levels";
import QuestionPage from "./question";

const GamePage = () => {
  var saisie;
  const questions = useStore("questions");
  const [showQuestionb, setShowQuestionb] = useState(store.state.showquestion);

  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  /* permet de filtrer les questions suivant le level choisi */

  function getAvailableQuestions() {
    if (store.state.level != "All continents") {
      store.dispatch("setAvailableQuestions", {
        value: questions.filter(
          (question) => question.continent[0] === store.state.level
        ),
      });
    } else {
      store.dispatch("setAvailableQuestions", { value: questions });
    }
  }

  /* permet de selectionner 10 indices sur les questions possibles suivant le niveau choisi */

  function getListOfIndices() {
    var indices = [];
    for (let pas = 0; pas < 10; pas++) {
      var indextoadd = getRandomIntInclusive(
        0,
        store.state.availablequestions.length-1
      );
      indices.push(indextoadd);
    }
    store.dispatch("setListOfIndices", { value: indices });
  }

  function pseudoenter() {
    saisie = document.getElementsByName("pseudo")[0].value;
    if (saisie.length > 0 && store.state.level != "") {
      setShowQuestionb(true);
      getAvailableQuestions();
      getListOfIndices();
      store.dispatch("setCurrentQuestion", {
        value: store.state.availablequestions[store.state.listofindices[0]],
      });
      store.state.pseudos.push(saisie);
      console.log(store.state.showquestion);
    }
  }


  return (
    <Page name="Jouer">
      <Navbar title="EnsaQuizz" />

      {showQuestionb ? (
        <Block className="card">
          <List>
            <QuestionPage />
          </List>
        </Block>
      ) : (
        <Block className="big-card">
          <PseudoPage />
          <LevelsPage />
          <Button className="button-play"
            onClick={function (e) {
              e.preventDefault();
              pseudoenter();
            }}
          >
            Play
          </Button>
        </Block>
      )}
    </Page>
  );
};

export default GamePage;
