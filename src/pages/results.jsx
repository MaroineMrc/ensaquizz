import { Block, Input, Page,List, useStore, Button } from "framework7-react";
import React from "react";
import BestScoresPage from "./bestscores";
import store from "../js/store";

const ResultsPage = () => {
  function rejouer() {
    store.dispatch("setIndice", { value: 1 });
    store.dispatch("setCurrentScore", { value: 0 });
    store.dispatch("setShowQuestion", {value : false});
  }

  
  return (
    <Page>
      <List>
        <h2 className="score-section">
          Your score is {store.state.currentscore} / 10
        </h2>
        <Button
          className="rejouer"
          onClick={function() {rejouer()}}
          href="/gamepage/"
        >
          Play again
        </Button>
        <BestScoresPage />
      </List>
    </Page>
  );
};

export default ResultsPage;
