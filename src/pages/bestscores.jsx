import { Block, Button } from "framework7-react";
import React, { useState } from "react";
import store from "../js/store";

const BestScoresPage = () => {
  const [showBestScore, setShowBestScore]=useState(false)
  return (
    <Block className="block-best-score">
      {showBestScore ? (
        <Block>
          <h3 className="best-score"> Best Scores :</h3>
          <div className="container">
            <p>
              {store.state.pseudos.map((element) => (
                <p>
                  {" "}
                  {element} <br />{" "}
                </p>
              ))}{" "}
            </p>
            <p>
              {store.state.scores.map((element) => (
                <p>
                  {" "}
                  {element} <br />{" "}
                </p>
              ))}
            </p>
          </div>
        </Block>
      ) : (
        <Button
          onClick={function () {
            setShowBestScore(true);
          }}
        >
          See top scores
        </Button>
      )}
    </Block>
  );
};

export default BestScoresPage;
