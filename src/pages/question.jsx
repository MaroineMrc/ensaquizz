import React, { useState } from "react";
import store from "../js/store";
import { Button, Block } from "framework7-react";
import ResultsPage from "./results";

const QuestionPage = () => {
  const [indice, setIndice] = useState(1);

  function incr() {
    store.dispatch("setCurrentQuestion", {
      value: store.state.availablequestions[store.state.listofindices[indice]],
    });
    setIndice(indice + 1);
  }

  function checkquestion(option) {
    if (option === store.state.currentquestion.answer) {
      store.dispatch("setCurrentScore", {
        value: store.state.currentscore + 1,
      });
    }
  }

  function editlistescore() {
    store.dispatch("setScores", { value: store.state.currentscore });
  }

  return (
    <>
      {console.log(store.state.availablequestions)}
      {console.log(store.state.listofindices)}
      <h1 className="numero-question">Question {indice} / 10 </h1>
      <h4 className="question-text">{store.state.currentquestion.statement}</h4>
      <div className="answer-section">
        {indice < 10 ? (
          store.state.currentquestion.options.map((option) => (
            <Button
              key={option}
              onClick={function () {
                checkquestion(option);
                incr();
              }}
            >
              {option}
            </Button>
          ))
        ) : (
          <>
            <Block>
              {store.state.currentquestion.options.map((option) => (
                <Button
                  key={option}
                  onClick={function () {
                    checkquestion(option);
                  }}
                >
                  {option}
                </Button>
              ))}
            </Block>
            <Block>
              <Button
                key="result"
                onClick={function () {
                  editlistescore();
                }}
              
                href="/resultspage/"
              >
                See results
              </Button>
            </Block>
          </>
        )}
      </div>
    </>
  );
};

export default QuestionPage;
