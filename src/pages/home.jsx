import React from 'react';
import {
  Page,
  Navbar,
  NavTitle,
  NavTitleLarge,
  Block,
  useStore} from 'framework7-react';

import store from '../js/store';


const HomePage = () => (
  
  <Page name="home">
    {/* Top Navbar */}
    <Navbar large sliding={false}>
      <NavTitle sliding>Ensaquizz</NavTitle>
      <NavTitleLarge>Ensaquizz</NavTitleLarge>
    </Navbar>

    {/* Page content */}
    <Block strong className='home-block'>
      <p>
        Welcome to the EnsaQuizz. 
        </p>

      <p>
        Here you can test your knowledge of the world's capitals while having fun. 
      </p>

      <p>
      Go to the Play section to start a game.
      </p>

    </Block>

  </Page>
);
export default HomePage;