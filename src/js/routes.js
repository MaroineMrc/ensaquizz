import HomePage from "../pages/home.jsx";
import GamePage from "../pages/jouer.jsx";
import DynamicRoutePage from "../pages/dynamic-route.jsx";
import RequestAndLoad from "../pages/request-and-load.jsx";
import NotFoundPage from "../pages/404.jsx";
import AboutUsPage from "../pages/aboutus.jsx";
import BestScoresPage from "../pages/bestscores.jsx";
import PseudoPage from "../pages/pseudo.jsx";
import LevelsPage from "../pages/levels.jsx";
import QuestionPage from "../pages/question.jsx";
import ResultsPage from "../pages/results.jsx";


var routes = [
  {
    path: "/levelspage/",
    component: LevelsPage,
  },
  {
    path: "/resultspage/",
    component: ResultsPage,
  },
  {
    path: "/questionpage/",
    component: QuestionPage,
  },
  {
    path: "/",
    component: HomePage,
  },
  {
    path: "/pseudopage/",
    component: PseudoPage,
  },
  {
    path: "/gamepage/",
    component: GamePage,
  },
  {
    path: "/bestscorespage/",
    component: BestScoresPage,
  },
  {
    path: "/aboutus/",
    component: AboutUsPage,
  },

  {
    path: "/dynamic-route/blog/:blogId/post/:postId/",
    component: DynamicRoutePage,
  },
  {
    path: "/request-and-load/user/:userId/",
    async: function ({ router, to, resolve }) {
      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = to.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: "Vladimir",
          lastName: "Kharlampidi",
          about: "Hello, i am creator of Framework7! Hope you like it!",
          links: [
            {
              title: "Framework7 Website",
              url: "http://framework7.io",
            },
            {
              title: "Framework7 Forum",
              url: "http://forum.framework7.io",
            },
          ],
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            component: RequestAndLoad,
          },
          {
            props: {
              user: user,
            },
          }
        );
      }, 1000);
    },
  },
  {
    path: "(.*)",
    component: NotFoundPage,
  },
];

export default routes;
