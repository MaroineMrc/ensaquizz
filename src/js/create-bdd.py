import requests
from random import *

url = "https://restcountries.com/v3.1/all"

reponse = requests.get(url)
contenu = reponse.json()
Pays = []
Capitale = []
Continents = []
for i in range (0,250):
    if "capital" in contenu[i] :
        Pays.append(contenu[i]["name"]["official"])
        Capitale.append(contenu[i]["capital"][0])
        Continents.append(contenu[i]["continents"])

'''
On veut un fichier de la forme :
{
    id: "1",
    statement: "Quel est la capitale de la France ?",
    answer: "Paris",
    options: ["Berlin", "Madrid","Paris", "Londres"],
}
'''
base_de_donnee=[]
for i in range (0,246):
    question = {"id" : i, "statement" : "What is the capital of ", "answer" : "", "options" : [], "continent" : ""}
    question["statement"]+= Pays[i] + "?"
    question["answer"]+= Capitale[i]
    question["continent"] = Continents[i]
    options = ["/","/", "/", "/"]
    n=randint(0,3)
    options[n]=Capitale[i]
    for k in range(0,4):
        if k != n : 
            p = randint(0, 245)
            if p != i and Capitale[p] not in options :
                options[k]=Capitale[p]
                
    question["options"]=options
    base_de_donnee.append(question)
    
import json

with open('bdd_questions.json', 'w') as f:
    json.dump(base_de_donnee, f)



