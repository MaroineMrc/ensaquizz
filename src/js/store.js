import { createStore } from "framework7/lite";
import bdd_questions from "./bdd_questions.json";


const store = createStore({
  state: {
    level : "",
    showquestion : false,
    indice: 1,
    questions: bdd_questions,
    listofindices :[],
    showscore : false,
    currentquestion: {},
    availablequestions :[],
    currentscore : 0,
    scores: [],
    pseudos: [],  },
  getters: {
    questions({ state }) {
      return state.questions;
    },
      level({ state }) {
        return state.level;
    },
    showquestion({state}) {
      return(state.showquestion)
    },
    showscore({state}) {
      return(state.showscore)
    },
    indice({state}) {
      return(state.indice)
    }
  },
  actions: {
    changeQuestion({ state }, question) {
      state.questions = [...state.questions, question];
    },
    setScores({ state }, {value}) {
      state.scores = [...state.scores, value];
    },
    setLevel({ state }, {value}) {
      state.level = value;
    },
    setCurrentQuestion({ state }, {value}) {
      state.currentquestion = value;
    },
    setAvailableQuestions({ state }, {value}) {
      state.availablequestions = value;
    },
    setListOfIndices({ state }, {value}) {
      state.listofindices = value;
    },
    setCurrentScore({ state }, {value}) {
      state.currentscore = value;
    },
    setShowScore({ state }, {value}) {
      state.showscore = value;
    },
    setShowQuestion({ state }, {value}) {
      state.showquestion = value;
    },
    setIndice({ state }, {value}) {
      state.indice = value;
    },
  },
});
export default store;
